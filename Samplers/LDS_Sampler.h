/*
 * LDS_Sampler.h
 *
 *  Created on: Sep 8, 2011
 *      Author: Rob
 */

#ifndef LDS_SAMPLER_H_
#define LDS_SAMPLER_H_

#include <vector>
#include "MCS_Sampler.h"
#include "UtilsMath.h"
#include "UtilsPHEV.h"
#include "UtilsGet.h"


class LDS_Sampler: public MCS_Sampler {
	public:
		LDS_Sampler();
		LDS_Sampler(int Ns);
		LDS_Sampler(pruning::SAMPLING_METHOD st);
		LDS_Sampler(pruning::SAMPLING_METHOD st, int Ns);
		virtual ~LDS_Sampler();

		virtual void run(MTRand& mt);
		void setNumSamples(int ni);

		double getNumSamples();
		
	private:
		int numSamples;
		pruning::SAMPLING_METHOD samplingType;
		std::vector < std::vector < double > > sampleMatrix;
		std::vector < std::vector < double > > linesMatrix;
};

#endif /* LDS_SAMPLER_H_ */
