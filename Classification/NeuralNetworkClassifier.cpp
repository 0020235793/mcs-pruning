/*
 * NeuralNetwork.cpp
 *
 *  Created on: Jan 18, 2011
 *      Author: rgreen
 */

#include "NeuralNetworkClassifier.h"
#include <iostream>
#include <iomanip>
#include <string.h>
using namespace std;

NeuralNetworkClassifier::NeuralNetworkClassifier() : Classifier() {
    trained 			= false;
    lineAdjustment 		= 1;
    numTrainingStates 	= 500;
    desiredAccuracy 	= .95;
    classifier 			= NULL;
    maxEpochs 			= 500;
    learningRate 		= 0.1;
    momentum 			= 0.1;
    verbose 			= false;
    nOutputs 			= 0;
}
NeuralNetworkClassifier::NeuralNetworkClassifier(std::string c, int n, std::vector < Generator > g, std::vector < Bus > b) : Classifier(c, n, g, b) {
    trained 			= false;
    lineAdjustment 		= 1;
    numTrainingStates 	= 500;
    nHidden 			= 33;
    desiredAccuracy 	= .95;
    classifier 			= NULL;
    maxEpochs 			= 500;
    learningRate 		= 0.1;
    momentum 			= 0.1;
    verbose 			= false;

    nInputs = gens.size()+1;
    nOutputs = 1;
}

NeuralNetworkClassifier::NeuralNetworkClassifier(std::string c, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > l) : Classifier(c, n, g, b, l) {
    trained 			= false;
    lineAdjustment 		= 1;
    numTrainingStates 	= 500;
    desiredAccuracy 	= .95;
    classifier 			= NULL;
    maxEpochs 			= 500;
    learningRate 		= 0.1;
    momentum 			= 0.1;
    verbose 			= false;
    classifier 			= NULL;

    nInputs  = gens.size()+lines.size()+1;
    nHidden  = nInputs*2;
    nOutputs = 1;
}

NeuralNetworkClassifier::NeuralNetworkClassifier(std::string c, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > l, Classifier* cl, int o, int nts, int nhn, double la) : Classifier (c, n, g, b, l){
    trained 			= false;
    lineAdjustment 		= 1;
    numTrainingStates 	= nts;
    nHidden 			= nhn;
    desiredAccuracy 	= .95;
    classifier 			= cl;
    maxEpochs 			= 500;
    learningRate 		= 0.001;
    momentum 			= 0.1;
    verbose 			= false;

    nInputs = gens.size()+lines.size()+1;
    nOutputs = o;
}

NeuralNetworkClassifier::~NeuralNetworkClassifier(){

}
void NeuralNetworkClassifier::reset(){
    trained = false;
    lineAdjustment = 1;
    numTrainingStates = 500;
    desiredAccuracy = 0.90;
    classifier = NULL;
    learningRate = 0.001;
    momentum = 0.9;

    Classifier::reset();
}

void NeuralNetworkClassifier::generateTrainingData(){
    double excess, retValue, stateProb, totalCap, curProb;
    MTRand mt;
    std::vector<double> curSolution(gens.size() + lines.size());
    std::vector<double> vInput(gens.size() + lines.size() + 1,0);
    std::vector<double> vOutput(1 ,0);;


    if(classifier == NULL){
        classifier = new OPFClassifier(curSystem, nb, gens, buses, lines);
        classifier->addLoad(addedLoad);
    }

    trainingInput.clear(); trainingOutput.clear();
    while(trainingInput.size() < (unsigned int)numTrainingStates){
        stateProb = 1.0; totalCap = 0;
        for(unsigned int i=0; i< gens.size(); i++){

            if(mt.rand() <=  gens[i].getOutageRate()){
                curSolution[i] 	= 0;
                vInput[i] 		= 0;
                stateProb 		*= gens[i].getOutageRate();
            }else{
                curSolution[i] 	= 1;
                vInput[i] 		= 1;
                totalCap 		+= gens[i].getPG();
                stateProb 		*= (1-gens[i].getOutageRate());
            }
        }
        for(unsigned int i=0; i<lines.size(); i++){
            if(mt.rand() <= (lines[i].getOutageRate() * lineAdjustment)){
                curSolution[gens.size()+i] 	= 0;
                vInput[gens.size()+i] 		= 0;
                stateProb 					*= (lines[i].getOutageRate());
            }else{
                curSolution[gens.size()+i] 	= 1;
                vInput[gens.size()+i] 		= 1;
                stateProb 					*= (1-lines[i].getOutageRate());
            }
        }

        retValue = classifier->run(curSolution, excess);
        vInput[gens.size() + lines.size()] 	= stateProb;

        if(retValue > 0){ vOutput[0] = 1; }
        else{ vOutput[0] = 0; }


        curProb = mt.rand();
        bool contains = false;
        if(curProb < 0.6){
            for(unsigned int i=0; i<trainingInput.size(); i++){
                if(trainingInput[i] == vInput){
                    contains = true;
                    break;
                }
            }
            if(!contains){
                trainingInput.push_back(vInput); trainingOutput.push_back(vOutput);
            }
        }else if(curProb < 0.8){
            validationInput.push_back(vInput); validationOutput.push_back(vOutput);
        }else{
            generalizationInput.push_back(vInput); generalizationOutput.push_back(vOutput);
        }
    }
}

void NeuralNetworkClassifier::initWeights(){
    MTRand mt;

    double rH = 1.0/sqrt((double)nInputs);
    double rO = 1.0/sqrt((double)nHidden);

    for(int i = 0; i <= nInputs; i++){
        for(int j = 0; j < nHidden; j++) {
            wInputHidden[i][j] = ( ((double)(rand()%100)+1)/100*2 * rH ) - rH;
        }
    }

    for(int i = 0; i <= nHidden; i++){
        for(int j = 0; j < nOutputs; j++) {
            //set weights to random values
            wHiddenOutput[i][j] = ( ( (double)(rand()%100)+1)/100 * 2 * rO ) - rO;
        }
    }
}
void NeuralNetworkClassifier::createNetwork(){

    inputNeurons.resize(nInputs+1, 0);
    inputNeurons[nInputs] = -1;

    hiddenNeurons.resize(nHidden+1, 0);
    hiddenNeurons[nInputs] = -1;

    outputNeurons.resize(nOutputs, 0);

    wInputHidden.resize(nInputs+1, std::vector<double>(nHidden, 0));
    wHiddenOutput.resize(nHidden+1, std::vector<double>(nOutputs, 0));

    deltaInputHidden.resize(nInputs+1, std::vector<double>(nHidden, 0));
    deltaHiddenOutput.resize(nHidden+1, std::vector<double>(nOutputs, 0));

    hiddenErrorGradients.resize(nHidden+1, 0);
    outputErrorGradients.resize(nOutputs+1, 0);
}
double NeuralNetworkClassifier::activationFunction(double x){
    return 1/(1 + exp(-x));
}
double NeuralNetworkClassifier::clampOutput(double x){
    if(x < 0.5) return 0;
    return 1;
}
void NeuralNetworkClassifier::feedForward(std::vector<double>& pattern){
    for(int i=0; i<nInputs; i++){
        inputNeurons[i] = pattern[i];
    }
    for(int j=0; j < nHidden; j++){
        hiddenNeurons[j] = 0;
        for(int i=0; i<= nInputs; i++){
            hiddenNeurons[j] += inputNeurons[i] * wInputHidden[i][j];
        }
        hiddenNeurons[j] = activationFunction(hiddenNeurons[j]);
    }
    for(int k=0; k < nOutputs; k++){
        outputNeurons[k] = 0;
        for(int j=0; j<=nHidden; j++){
            outputNeurons[k] += hiddenNeurons[j] * wHiddenOutput[j][k];
        }
        outputNeurons[k] = activationFunction(outputNeurons[k]);
    }
}
double NeuralNetworkClassifier::getOutputErrorGradient(double desiredValue, double outputValue){
    return outputValue*(1-outputValue)*(desiredValue-outputValue);
}
double NeuralNetworkClassifier::getHiddenErrorGradient(int j){
    double weightedSum = 0;
    for(int k=0; k<nOutputs; k++ ){
        weightedSum += wHiddenOutput[j][k] * outputErrorGradients[k];
    }
    return hiddenNeurons[j]*(1-hiddenNeurons[j])*weightedSum;
}
void NeuralNetworkClassifier::updateWeights(){
    for (int i = 0; i <= nInputs; i++){
        for (int j = 0; j < nHidden; j++) {
            wInputHidden[i][j] += deltaInputHidden[i][j];
        }
    }
    for (int i = 0; i <= nHidden; i++){
        for (int j = 0; j < nOutputs; j++) {
            wHiddenOutput[i][j] += deltaHiddenOutput[i][j];
        }
    }
}
void NeuralNetworkClassifier::backPropagate(std::vector<double>& desiredOutput){
    for (int k = 0; k < nOutputs; k++){
        outputErrorGradients[k] = getOutputErrorGradient(desiredOutput[k], outputNeurons[k]);
        for (int j=0; j<=nHidden; j++){
            deltaHiddenOutput[j][k] = learningRate * hiddenNeurons[j] * outputErrorGradients[k] + momentum * deltaHiddenOutput[j][k];
        }
    }
    for (int j = 0; j < nHidden; j++){
        hiddenErrorGradients[j] = getHiddenErrorGradient(j);
        for (int i = 0; i <= nInputs; i++){
            deltaInputHidden[i][j] = learningRate * inputNeurons[i] * hiddenErrorGradients[j] + momentum * deltaInputHidden[i][j];
        }
    }
}
void NeuralNetworkClassifier::train(){
    double trainingSetAccuracy, trainingSetMSE, incorrectPatterns, mse;
    double generalizationSetAccuracy, generalizationSetMSE;
    bool patternCorrect;

    timer.startTimer();
    if(verbose){
        std::cout << setw(20) << "Learning Rate: " 		<< setw(5) << setprecision(2) << learningRate << std::endl;
        std::cout << setw(20) << "Momentum: " 			<< setw(5) << setprecision(2) << momentum << std::endl;
        std::cout << setw(20) << "Desired Accuracy: " 	<< setw(5) << setprecision(2) << desiredAccuracy << std::endl;
        std::cout << std::endl;

        std::cout << setw(10) << "Epoch"
             << setw(20) << "Desired Accuracy"
             << setw(20) << "Training Accuracy"
             << setw(20) << "Training MSE"
             << setw(20) << "Gen. Accuracy"
             << setw(20) << "Gen. MSE "
             << std::endl;
    }

    for(int j=0; j<maxEpochs; j++){
        if(trainingSetAccuracy > desiredAccuracy && generalizationSetAccuracy > desiredAccuracy) break;

        incorrectPatterns = 0;
        mse = 0;

        for(int i=0; i<(int)trainingInput.size(); i++){

            feedForward(trainingInput[i]);
            backPropagate(trainingOutput[i]);
            updateWeights();

            patternCorrect = true;
            for(int k = 0; k<nOutputs; k++){
                if(clampOutput(outputNeurons[k]) != trainingOutput[i][k]){
                    patternCorrect = false;
                }
                mse += pow((outputNeurons[k]-trainingOutput[i][k]), 2);
            }
            if (!patternCorrect) incorrectPatterns++;

        }//end for

        trainingSetAccuracy 		= 1.0 - (incorrectPatterns/(double)trainingInput.size());
        trainingSetMSE 				= mse /(nOutputs*(double)trainingInput.size());
        generalizationSetAccuracy 	= getSetAccuracy(generalizationInput, generalizationOutput);
        generalizationSetMSE 		= getSetMSE(generalizationInput, generalizationOutput);

        if(verbose){
            std::cout << setw(10)  << setprecision(4) << j;
            std::cout << setw(20) << setprecision(4) << desiredAccuracy;
            std::cout << setw(20) << setprecision(4) << trainingSetAccuracy;
            std::cout << setw(20) << setprecision(4) << trainingSetMSE;
            std::cout << setw(20) << setprecision(4) << generalizationSetAccuracy;
            std::cout << setw(20) << setprecision(4) << generalizationSetMSE;
            std::cout << std::endl;
        }
    }

    if(verbose){
        std::cout << setw(20) << " Validation Accuracy:" << setw(20) << setprecision(4) << getSetAccuracy(validationInput, validationOutput) << std::endl;
        std::cout << setw(20) << " Validation MSE:" 		<< setw(20) << setprecision(4) << getSetMSE(validationInput, validationOutput) << std::endl;
    }

    trained = true;
    timer.stopTimer();
    trainingTime += timer.getElapsedTime();
}
double NeuralNetworkClassifier::getSetAccuracy(std::vector< std::vector < double > >& input, std::vector< std::vector < double > >& output){
    double incorrectResults = 0;
    bool correctResult;

    for(unsigned int i=0; i<input.size(); i++){
        feedForward(input[i]);
        correctResult = true;

        for(unsigned int j=0; j<outputNeurons.size(); j++){
            if(clampOutput(outputNeurons[j]) != output[i][j]){
                correctResult = false;
            }
        }
        if(!correctResult) incorrectResults++;
    }
    return 1.0 - (incorrectResults/input.size());
}
double NeuralNetworkClassifier::getSetMSE(std::vector< std::vector < double > >& input, std::vector< std::vector < double > >& output){
    double mse = 0;
    for(unsigned int i=0; i<input.size(); i++){
        feedForward(input[i]);
        for(int j=0; j<nOutputs; j++){
            mse += pow((outputNeurons[j] - output[i][j]), 2);
        }
    }
    return mse/(nOutputs * input.size());
}

void NeuralNetworkClassifier::init(){
    stringstream ss(stringstream::in | stringstream::out);
    string fName;

    createNetwork();
    initWeights();
    generateTrainingData();
    train();

    trained = true;
}

double NeuralNetworkClassifier::classify(std::vector<double> curSolution, double& excess){
    double retValue;
    if(!trained) train();

    timer.startTimer();

    feedForward(curSolution);
    retValue = clampOutput(outputNeurons[0]);

    timer.stopTimer();
    classificationTime += timer.getElapsedTime();

    return retValue;
}

double NeuralNetworkClassifier::classify(std::string curSolution, double& excess){
    std::vector<double> newSolution(curSolution.size());

    for(unsigned int i=0; i<curSolution.size(); i++){
        newSolution[i] = atof(&curSolution[i]);
    }
    return classify(newSolution, excess);
}
double NeuralNetworkClassifier::classify(std::vector<int> curSolution, double& excess){
    std::vector<double> newSolution(curSolution.size(),0);

    for(unsigned int i=0; i<curSolution.size(); i++){
        newSolution[i] = curSolution[i];
    }
    return classify(newSolution, excess);
}

void NeuralNetworkClassifier::load(){
    if(!trained) train();
    train();
}
double NeuralNetworkClassifier::run(std::vector<double> curSolution, double& excess){
    return classify(curSolution, excess);
}
double NeuralNetworkClassifier::run(std::string curSolution, double& excess){
    return classify(curSolution, excess);
}

bool NeuralNetworkClassifier::loadWeights(std::string filename){
    //open file for reading
    std::fstream inputFile;
    inputFile.open(filename.c_str(), ios::in);


    if(inputFile.is_open()){
        std::vector<double> weights;
        string line = "";

        createNetwork();
        //read data
        while(!inputFile.eof()){
            getline(inputFile, line);

            //process line
            if(line.length() > 2){
                //store inputs
                char* cstr = new char[line.size()+1];
                char* t;
                strcpy(cstr, line.c_str());

                //tokenise
                int i = 0;
                t=strtok (cstr,",");

                while(t!=NULL){
                    weights.push_back( atof(t) );
                    t = strtok(NULL,",");
                    i++;
                }
                delete cstr;
            }
        }

        //check if sufficient weights were loaded
        if((int)weights.size() != ((nInputs+1) * nHidden + (nHidden+1) * nOutputs )){
            if(verbose){
                std::cout << std::endl << "Error - Incorrect number of weights in input file: " << filename << std::endl;
                exit(1);
            }
            inputFile.close();
            return false;
        }else{
            //set weights
            int pos = 0;

            for(int i=0; i<=nInputs; i++){
                for(int j=0; j<nHidden; j++){
                    wInputHidden[i][j] = weights[pos++];
                }
            }

            for(int i=0; i<=nHidden; i++){
                for(int j=0; j<nOutputs; j++){
                    wHiddenOutput[i][j] = weights[pos++];
                }
            }

            if(verbose){
                std::cout << std::endl << "Neuron weights loaded successfully from '" << filename << "'" << std::endl;
            }
            inputFile.close();
            trained = true;
            return true;
        }
    }else{
        if(verbose){
            std::cout << std::endl << "Error - Weight input file '" << filename << "' could not be opened: " << std::endl;
            exit(1);
        }
        return false;
    }
}

bool NeuralNetworkClassifier::saveWeights(std::string filename){
    //open file for reading
    std::fstream outputFile;
    outputFile.open(filename.c_str(), ios::out);

    if(outputFile.is_open()){
        outputFile.precision(50);

        //output weights
        for(int i=0; i<=nInputs; i++){
            for(int j=0; j<nHidden; j++){
                outputFile << wInputHidden[i][j] << ",";
            }
        }

        for(int i=0; i<=nHidden; i++){
            for(int j=0; j<nOutputs; j++){
                outputFile << wHiddenOutput[i][j];
                if(i*nOutputs+j+1 != (nHidden+1) *nOutputs) outputFile << ",";
            }
        }

        //print success
        if(verbose){
            std::cout << std::endl << "Neuron weights saved to '" << filename << "'" << std::endl;
        }

        //close file
        outputFile.close();

        return true;
    }else{
        if(verbose){
            std::cout << std::endl << "Error - Weight output file '" << filename << "' could not be created: " << std::endl;
        }
        return false;
    }
}
void NeuralNetworkClassifier::setNumHiddenNeurons	(int nhn) 	{ nHidden = nhn; }
void NeuralNetworkClassifier::setNumTrainingStates(int nts) 	{ numTrainingStates = nts; }
void NeuralNetworkClassifier::setDesiredAccuracy	(double da) { desiredAccuracy = da; }
void NeuralNetworkClassifier::setLearningRate		(double lr) { learningRate = lr; }
void NeuralNetworkClassifier::setMomentum			(double m) 	{ momentum = m; }
void NeuralNetworkClassifier::setVerbose			(bool v) 	{ verbose = v; }
void NeuralNetworkClassifier::setNumOutputs		(int no)	{ nOutputs = no; }
double NeuralNetworkClassifier::getTrainingTime() { return trainingTime;}
double NeuralNetworkClassifier::getClassificationTime() { return trainingTime;}
