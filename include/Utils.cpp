#include "UtilsVector.h"
#include "UtilsSampling.h"
#include "UtilsString.h"
#include "UtilsMath.h"
#include "UtilsSystem.h"
#include "UtilsLogging.h"
#include "UtilsPHEV.h"
#include "UtilsGet.h"
#include "UtilsCommandLine.h"




#include <iostream>
#include "Eigen/Dense"


namespace UtilsCommandLine{
    void setUsage(AnyOption* opt){
        // General
        opt->addUsage( "" );
        opt->addUsage( "Usage: " );
        opt->addUsage( "" );
        opt->addUsage( "--help              Prints this help");
        opt->addUsage( "" );

        // Parallelization
        opt->addUsage("--numThreads        # of threads to use with OpenMP");   
        opt->addUsage("--batchSize         # of samples per batch used in MCS Sampling with OpenMP or CUDA");
        
        // Parallel Batch OpenMP
        opt->addUsage("--genThreads        For use with OpenMP Pipeline. Number of Generation Threads.");
        opt->addUsage("--classThreads      For use with OpenMP Pipeline. Number of Classification Threads.");

        // Power System Specific:wq
        opt->addUsage("--system            RTS79, MRTS, or RTS96. Defaults to RTS79");
        opt->addUsage("--useLines          Use Transmission Lines");
        opt->addUsage("--usePHEVs          Use PHEV formulation");
        opt->addUsage("--lineAdj           Multiplied with line FOR. Defaults to 1.0");
        opt->addUsage("--saveGenOutages    Save Generator Outage Stats");
        opt->addUsage("--saveLineOutages   Save Line Outage Stats");
        opt->addUsage("" );

        // PHEV Specific
        opt->addUsage("--penetration       Penetration level for PHEVs");
        opt->addUsage("--rho               Rho value for PHEVs");
        opt->addUsage("--totalVehicles     Total Vehicles for PHEVs");
        opt->addUsage("--placement         PHEV Placement - EvenAllBuses, EvenLoadBuses, RandomAllBuses, RandomLoadBuses, FairDistribution");
        opt->addUsage("" );

        // Classification
        opt->addUsage("--classifier        CAP, SVM, NN, or OPF. Defaults to OPF");
        opt->addUsage("" );
        
        // Sampling
        opt->addUsage("--sampler           MCS, LHS, DS, HAL, HAM, FAU, SOB, or PI. Defaults to MCS.");
        opt->addUsage("--samplingPop       Sampling population size. Defaults to 25.");
        opt->addUsage("--samplingGen       Sampling number of generations. Defaults to 25.");
        opt->addUsage("--sigma             Tolerance for convergance");
        opt->addUsage("--useMOSampler      Use Multi-Objective Sampler");
        opt->addUsage("--numSamples        Number of experiments for LHS");
        opt->addUsage("" );

        // Pruning
        opt->addUsage("--pruner            MCS, AIS, GA, MGA, PSO, or ACO. Defaults to MCS.");
        opt->addUsage("--useMOPruner       Use Multi-Objective Pruner");
        opt->addUsage("--useWeightedMO     Use Weighted version of Multi-Objective Pruner");
        opt->addUsage("--useParetoMO       Use Pareto version of Multi-Objective Pruner");
        opt->addUsage("--useLocalSearch    Use Local Search Algorithm");
        opt->addUsage("" );
        opt->addUsage("--logConvergence    Log Convergence Characteristics");
        opt->addUsage("--negateFitness     Negate Fitness Function");
        opt->addUsage("--pruningObj        Prob, Curt, Excess, or Copy. Defaults to Prob");
        opt->addUsage("" );
        opt->addUsage("--pruningPopMin     Min population sizeDefaults to 25.");
        opt->addUsage("--pruningPopMax     Max population sizeDefaults to 25.");
        opt->addUsage("--pruningPopStep    Incremental value from popMin to popMaxDefaults to 5.");
        opt->addUsage("" );
        opt->addUsage("--pruningGenMin     Min number of generationsDefaults to 25.");
        opt->addUsage("--pruningGenMax     Max number of generationsDefaults to 25.");
        opt->addUsage("--pruningGenStep    Incremental value from genMin to genMax. Defaults to 5.");
        opt->addUsage("" );
        opt->addUsage("--stoppingMethod    MaxProbPruned, MinProbSlope, MaxStatesPruned, MinStatesSlope, or Generation. Defaults to Generation.");
        opt->addUsage("--stoppingValue     Used in conjection with stoppingMethod.");
        opt->addUsage("--trials            Trials per run");

    }
    void setOptions(AnyOption* opt){
        // Flags
        opt->setFlag("help", 'h');

        // Parallel
        opt->setOption("batchSize"); 
        opt->setOption("numThreads");

        // Parallel Batch OpenMP
        opt->setOption("genThreads");
        opt->setOption("classThreads");

        // Power System
        opt->setOption("system");
        opt->setOption("lineAdj");
        opt->setFlag("useLines");
        opt->setFlag("usePHEVs");
        opt->setFlag("saveGenOutages");
        opt->setFlag("saveLineOutages");

        // PHEV Options
        opt->setOption("penetration");
        opt->setOption("rho");
        opt->setOption("totalVehicles");
        opt->setOption("placement");

        // Classifying
        opt->setOption("classifier");

        // Sampling
        opt->setOption("sampler");      
        opt->setOption("samplingPop");  
        opt->setOption("samplingGen");  
        opt->setOption("sigma");
        opt->setOption("numSamples");

        // Pruning
        opt->setFlag("logConvergence");
        opt->setFlag("negateFitness");
        opt->setFlag("useMOPruner");
        opt->setFlag("useWeightedMO");
        opt->setFlag("useParetoMO");
        opt->setFlag("useLocalSearch");

        opt->setOption("pruner");
        opt->setOption("pruningObj");
        opt->setOption("trials");
        opt->setOption("stoppingMethod");
        opt->setOption("stoppingValue");
        opt->setOption("pruningPopMin"); opt->setOption("pruningPopMax"); opt->setOption("pruningPopStep");
        opt->setOption("pruningGenMin"); opt->setOption("pruningGenMax"); opt->setOption("pruningGenStep");
    }
}//end UtilsCommandLine

namespace UtilsGet{
    std::string getPruningMethodString(pruning::PRUNING_METHOD pm){
        std::string retValue;
        switch(pm){
            case pruning::PM_ACO: retValue = "ACO"; break;
            case pruning::PM_AIS: retValue = "AIS"; break;
            case pruning::PM_GA:  retValue = "GA";  break;
            case pruning::PM_MGA: retValue = "MGA"; break;
            case pruning::PM_PSO: retValue = "PSO"; break;
            case pruning::PM_RPSO:retValue = "RPSO";break;
            case pruning::PM_CFO: retValue = "CFO"; break;
            default: retValue = "NONE";
        }
        return retValue;
    }
    std::string getClassificationMethodString(pruning::CLASSIFICATION_METHOD cm){
        std::string retValue;
        switch(cm){
            case pruning::CM_NN:  retValue = "NN";  break;
            case pruning::CM_OPF: retValue = "OPF"; break;
            case pruning::CM_SVM: retValue = "SVM"; break;
            case pruning::CM_CAP: retValue = "CAP"; break;
            default: retValue = "OPF";
        }
        return retValue;
    }
    std::string getSamplingMethodString(pruning::SAMPLING_METHOD ps){
        std::string retValue;
        switch(ps){
            case pruning::SM_ACO:       retValue = "ACO";             break;
            case pruning::SM_AIS:       retValue = "AIS";             break;
            case pruning::SM_GA:        retValue = "GA";              break;
            case pruning::SM_MGA:       retValue = "MGA";             break;
            case pruning::SM_PSO:       retValue = "PSO";             break;
            case pruning::SM_RPSO:      retValue = "RPSO";            break;
            case pruning::SM_MO_PSO:    retValue = "MO_PSO";          break;
            case pruning::SM_LHS:       retValue = "LHS";             break;
            case pruning::SM_DS:        retValue = "DS";              break;
            case pruning::SM_HAL:       retValue = "HAL";             break;
            case pruning::SM_HAM:       retValue = "HAM";             break;
            case pruning::SM_FAU:       retValue = "FAU";             break;
            case pruning::SM_CFO:       retValue = "CFO";             break;
            case pruning::SM_PI:        retValue = "PI";              break;
            case pruning::SM_SOBOL:     retValue = "SOBOL";           break;
            case pruning::SM_ATV:       retValue = "ATV";             break;
            default: retValue = "MCS";
        }
        return retValue;
    }
    std::string getBooleanString(bool value){
        if(value) { return "True";}
        else      { return "False";}
    }
    std::string getPHEVPlacementString(pruning::PHEV_PLACEMENT pp){
        std::string retValue;
        switch(pp){
            case pruning::PP_NONE:              retValue = "NONE";             break;
            case pruning::PP_EVEN_ALL_BUSES:    retValue = "EvenAllBuses";     break;
            case pruning::PP_EVEN_LOAD_BUSES:   retValue = "EvenLoadBuses";    break;
            case pruning::PP_RANDOM_ALL_BUSES:  retValue = "RandomAllBuses";   break;
            case pruning::PP_RANDOM_LOAD_BUSES: retValue = "RandomLoadBuses";  break;
            case pruning::PP_FAIR_DISTRIBUTION: retValue = "FairDistribution"; break;
            default: retValue = "NONE";
        }
        return retValue;
    }
    std::string getPruningObjString(pruning::PRUNING_OBJECTIVE po){
        std::string retValue;
        switch(po){
            case pruning::PO_PROBABILITY :  retValue = "Probability";   break;
            case pruning::PO_CURTAILMENT:   retValue = "Curtailment";   break;
            case pruning::PO_COPY:          retValue = "Copy";          break;
            case pruning::PO_EXCESS:        retValue = "Excess";        break;
            case pruning::PO_ZERO:          retValue = "Zero";          break;
            case pruning::PO_TUP:           retValue = "Tup";           break;
            default: retValue = "Probability";
        }
        return retValue;
    }
    std::string getStoppingMethodString(pruning::STOPPING_METHOD sm){
        std::string retValue;
        switch(sm){
            case pruning::SM_STATES_SLOPE:  retValue = "MinStatesSlope";    break;
            case pruning::SM_STATES_PRUNED: retValue = "MaxStatesPruned";   break;
            case pruning::SM_PROB_MAX:      retValue = "MaxProbPruned";     break;
            case pruning::SM_PROB_SLOPE:    retValue = "MinProbSlope";      break;
            case pruning::SM_GENERATION:    retValue = "Generation";        break;
            default: retValue = "";
        }
        return retValue;
    }

    pruning::PRUNING_METHOD getPruningMethod(std::string s){
        pruning::PRUNING_METHOD retValue;
        s = UtilsSampling::toUpper(s);
        if(s == "ACO")      {retValue = pruning::PM_ACO;}
        else if(s == "AIS") {retValue = pruning::PM_AIS;}
        else if(s == "GA")  {retValue = pruning::PM_GA;}
        else if(s == "MGA") {retValue = pruning::PM_MGA;}
        else if(s == "PSO") {retValue = pruning::PM_PSO;}
        else if(s == "RPSO"){retValue = pruning::PM_RPSO;}
        else if(s == "CFO") {retValue = pruning::PM_CFO;}
        else                {retValue = pruning::PM_NONE;}
        return retValue;
    }
    pruning::CLASSIFICATION_METHOD getClassificationMethod(std::string s){
        pruning::CLASSIFICATION_METHOD retValue;
        s = UtilsSampling::toUpper(s);
        if(s == "NN")       {retValue = pruning::CM_NN;}
        else if(s == "OPF") {retValue = pruning::CM_OPF;}
        else if(s == "SVM") {retValue = pruning::CM_SVM;}
        else if(s == "CAP") {retValue = pruning::CM_CAP;}
        else                {retValue = pruning::CM_OPF;}
        return retValue;
    }
    pruning::SAMPLING_METHOD getSamplingMethod(std::string s){
        pruning::SAMPLING_METHOD retValue;
        s = UtilsSampling::toUpper(s);
        
        if(s == "ACO")                      {retValue = pruning::SM_ACO;}
        else if(s == "AIS")                 {retValue = pruning::SM_AIS;}
        else if(s == "GA")                  {retValue = pruning::SM_GA;}
        else if(s == "MGA")                 {retValue = pruning::SM_MGA;}
        else if(s == "PSO")                 {retValue = pruning::SM_PSO;}
        else if(s == "RPSO")                {retValue = pruning::SM_RPSO;}
        else if(s == "MO_PSO")              {retValue = pruning::SM_MO_PSO;}
        else if(s == "LHS")                 {retValue = pruning::SM_LHS;}
        else if(s == "DS")                  {retValue = pruning::SM_DS;}
        else if(s == "HAL")                 {retValue = pruning::SM_HAL;}
        else if(s == "HAM")                 {retValue = pruning::SM_HAM;}
        else if(s == "FAU")                 {retValue = pruning::SM_FAU;}
        else if(s == "CFO")                 {retValue = pruning::SM_CFO;}
        else if(s == "PI")                  {retValue = pruning::SM_PI;}
        else if(s == "SOB")                 {retValue = pruning::SM_SOBOL;}
        else if(s == "ATV")                 {retValue = pruning::SM_ATV;}
        else                                {retValue = pruning::SM_MCS;}
        return retValue;
    }
    pruning::PHEV_PLACEMENT getPHEVPlacement(std::string s){
        pruning::PHEV_PLACEMENT retValue;
        s =UtilsSampling::toUpper(s);
        if(s == "EVENALLBUSES")         {retValue = pruning::PP_EVEN_ALL_BUSES;}
        else if(s == "EVENLOADBUSES")   {retValue = pruning::PP_EVEN_LOAD_BUSES;}
        else if(s == "RANDOMALLBUSES")  {retValue = pruning::PP_RANDOM_ALL_BUSES;}
        else if(s == "RANDOMLOADBUSES") {retValue = pruning::PP_RANDOM_LOAD_BUSES;}
        else if(s == "FAIRDISTRIBUTION"){retValue = pruning::PP_FAIR_DISTRIBUTION;}
        else                            {retValue = pruning::PP_NONE;}
        return retValue;
    }
    pruning::STOPPING_METHOD getStoppingMethod(std::string s){
        pruning::STOPPING_METHOD retValue;
        s = UtilsSampling::toUpper(s);
        if(s == "MINSTATESSLOPE")       {retValue = pruning::SM_STATES_SLOPE;}
        else if(s == "MAXSTATESPRUNED") {retValue = pruning::SM_STATES_PRUNED;}
        else if(s == "MAXPROBPRUNED")   {retValue = pruning::SM_PROB_MAX;}
        else if(s == "MINPROBSLOPE")    {retValue = pruning::SM_PROB_SLOPE;}
        else if(s == "GENERATION")      {retValue = pruning::SM_GENERATION;}
        else                            {retValue = pruning::SM_GENERATION;}
        return retValue;
    }
    pruning::PRUNING_OBJECTIVE getPruningObj(std::string s){
        pruning::PRUNING_OBJECTIVE retValue;
        s = UtilsSampling::toUpper(s);
        if(s == "PROBABILITY")      {retValue = pruning::PO_PROBABILITY;}
        else if(s == "PROB")        {retValue = pruning::PO_PROBABILITY;}
        else if(s == "CURTAILMENT") {retValue = pruning::PO_CURTAILMENT;}
        else if(s == "CURT")        {retValue = pruning::PO_CURTAILMENT;}
        else if(s == "COPY")        {retValue = pruning::PO_COPY;}
        else if(s == "EXCESS")      {retValue = pruning::PO_EXCESS;}
        else if(s == "ZERO")        {retValue = pruning::PO_ZERO;}
        else if(s == "TUP")         {retValue = pruning::PO_TUP;}
        else                        {retValue = pruning::PO_PROBABILITY;}
        
        return retValue;
    }

}//end UtilsGet

namespace UtilsPHEV{
    void calculatePHEVLoad(
            double penetrationLevel, double rho, int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT phevPlacement){

        double mean, stdDev;
        double tempK, tempBC;
        double iStar;
        double chargeDischargeDistance, numClasses;
        std::vector<double> numPHEVs;
        std::vector<double> Ae, Be,         BC, K,
                            meanK,meanBC,   sigmaK,sigmaBC,
                            kMin, kMax,     bcMin, bcMax,
                            vehicleProbs,   milesDriven,
                            neededEnergy,   energyPerMile,
                            departureTime,  arrivalTime,
                            rechargeLength, rechargeVoltage,
                            rechargeCurrent,rechargeTime,
                            loadPerClass;
        std::vector < std::vector <double> > epsilon;

        numClasses = 4, chargeDischargeDistance = 40;

        numPHEVs.resize(numClasses, 0);
        meanK   .resize(numClasses, 0);         meanBC  .resize(numClasses, 0);
        sigmaK  .resize(numClasses, 0);         sigmaBC .resize(numClasses, 0);
        kMin    .resize(numClasses, 0);         kMax    .resize(numClasses, 0);
        bcMin   .resize(numClasses, 0);         bcMax   .resize(numClasses, 0);
        BC      .resize(numClasses, 0);         K       .resize(numClasses, 0);
        Ae      .resize(numClasses, 0);         Be      .resize(numClasses, 0);

        vehicleProbs    .resize(numClasses, 0);
        milesDriven     .resize(numClasses, 0);
        neededEnergy    .resize(numClasses, 0);
        energyPerMile   .resize(numClasses, 0);
        departureTime   .resize(numClasses, 0);
        arrivalTime     .resize(numClasses, 0);
        rechargeLength  .resize(numClasses, 0);
        rechargeVoltage .resize(numClasses, 0);
        rechargeCurrent .resize(numClasses, 0);
        rechargeTime    .resize(numClasses, 0);
        loadPerClass    .resize(numClasses, 0);
        epsilon         .resize(numClasses, std::vector<double>(numClasses,0));

        vehicleProbs[0] = .2; vehicleProbs[1] = .3;
        vehicleProbs[2] = .3; vehicleProbs[3] = .2;

        Ae[0]    = 0.3790; Ae[1]    = 0.4288; Ae[2]    = 0.6720; Ae[3]   = 0.8180;
        Be[0]    = 0.4541; Be[1]    = 0.4179; Be[2]    = 0.4040; Be[3]   = 0.4802;
        kMin[0]  = .2447;  kMin[1]  = .2750;  kMin[2]  = .3217;  kMin[3]  = .3224;
        kMax[0]  = 0.5976; kMax[1]  = 0.6151; kMax[2]  = 0.5428; kMax[3]  = 0.4800;
        bcMax[0] = 12;     bcMax[1] = 14;     bcMax[2] = 21;     bcMax[3] = 23;
        bcMin[0] = 8;      bcMin[1] = 10;     bcMin[2] = 17;     bcMin[3] = 19;

        for(int i=0; i<numClasses; i++){
            meanK[i]   = (kMax[i]  + kMin[i])  /2;
            meanBC[i]  = (bcMax[i] + bcMin[i]) /2;
            sigmaK[i]  = (kMax[i]  - kMin[i])  /4;
            sigmaBC[i] = (bcMax[i] - bcMin[i]) /4;
        }

        // During Each MCS Iteration
        for(int i=0; i<numClasses; i++){
            // Calculate Number of PHEVS per Class
            mean        = totalVehicles*penetrationLevel*vehicleProbs[i];
            stdDev      = .01 * mean;
            numPHEVs[i] = mt.randNorm(mean, stdDev);

            //Calculate total # of V2G?
            epsilon[0][0] = sigmaK[i]*sigmaK[i];
            epsilon[0][1] = rho*sigmaK[i]*sigmaBC[i];
            epsilon[1][0] = rho*sigmaK[i]*sigmaBC[i];
            epsilon[1][1] = sigmaBC[i]*sigmaBC[i];
            UtilsMath::twoByTwoCholeskyDecomp(epsilon);

            // Move the following to invidiual cars
            //gsl_ran_bivariate_gaussian(r, sigmaK[i], sigmaBC[i], rho, &tempK, &tempBC);
            RandomNumbers::bivariateGaussian(mt, sigmaK[i], sigmaBC[i], rho, tempK, tempBC);
            K[i]  = meanK[i] + tempK;
            BC[i] = meanBC[i] + tempBC;

            // Calculate Miles driven per class
            //milesDriven[i] = gsl_ran_lognormal(r, 3.37, 0.5);
            milesDriven[i] = RandomNumbers::logNormal(mt, 3.37, 0.5);

            //Arrival and Departure Time
            departureTime[i] = mt.randNorm(7,  sqrt(3.0));
            arrivalTime[i]   = mt.randNorm(18, sqrt(3.0));

            // Energy Needed
            if(milesDriven[i] > chargeDischargeDistance){ // full Charge Required
                neededEnergy[i] = BC[i];
            }else{
                neededEnergy[i] = (milesDriven[i] * (Ae[i] * pow(K[i],Be[i])));
            }

            // Compute Recharge Length
            rechargeLength[i] = (24.0-arrivalTime[i]) + departureTime[i];

            // Compute Voltage needed
            if(mt.rand() < 0.70){
                rechargeVoltage[i] = 120;
                iStar = 15;
            }else{
                rechargeVoltage[i] = 240;
                iStar = 30;
            }
            // Compute current needed
            rechargeCurrent[i] = std::min((milesDriven[i]*1000)/(rechargeVoltage[i]*rechargeLength[i]), iStar);

            // Compute total power in MW in a single hour
            loadPerClass[i] = (numPHEVs[i]*rechargeVoltage[i]*rechargeCurrent[i])/1000000;
        }

        //Distributed Load
        phevLoad.clear(); phevGen.clear();
        phevLoad.resize(numBuses,0); phevGen.resize(numBuses,0);

        if(phevPlacement == pruning::PP_EVEN_ALL_BUSES){
            //Evenly at All Buses
            for(int i=0; i<numBuses; i++){
                phevLoad[i] = 0;
                for(int j=0; j<numClasses; j++){
                    phevLoad[i] += loadPerClass[j]/(double)numBuses;
                }
            }
        }else if(phevPlacement  == pruning::PP_EVEN_LOAD_BUSES){
            // Currently handles only RTS79 with 18 Buses
            for(int j=0; j<numClasses; j++){
                phevLoad[0]  += loadPerClass[j]/18.0;
                phevLoad[1]  += loadPerClass[j]/18.0;
                phevLoad[2]  += loadPerClass[j]/18.0;
                phevLoad[3]  += loadPerClass[j]/18.0;
                phevLoad[4]  += loadPerClass[j]/18.0;
                phevLoad[5]  += loadPerClass[j]/18.0;
                phevLoad[6]  += loadPerClass[j]/18.0;
                phevLoad[7]  += loadPerClass[j]/18.0;
                phevLoad[8]  += loadPerClass[j]/18.0;
                phevLoad[9]  += loadPerClass[j]/18.0;
                phevLoad[10] += loadPerClass[j]/18.0;
                phevLoad[13] += loadPerClass[j]/18.0;
                phevLoad[14] += loadPerClass[j]/18.0;
                phevLoad[15] += loadPerClass[j]/18.0;
                phevLoad[16] += loadPerClass[j]/18.0;
                phevLoad[18] += loadPerClass[j]/18.0;
                phevLoad[19] += loadPerClass[j]/18.0;
                phevLoad[20] += loadPerClass[j]/18.0;
            }
        }else if(phevPlacement  == pruning::PP_RANDOM_ALL_BUSES){
            //Evenly at All Buses
            for(int i=0; i<numBuses; i++){
                phevLoad[i] = 0;
                for(int j=0; j<numClasses; j++){
                    phevLoad[i] += loadPerClass[j]/(double)numBuses * mt.rand();
                }
            }
        }else if(phevPlacement  == pruning::PP_RANDOM_LOAD_BUSES){
            for(int j=0; j<numClasses; j++){
                phevLoad[0]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[1]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[2]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[3]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[4]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[5]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[6]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[7]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[8]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[9]  += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[10] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[13] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[14] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[15] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[16] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[18] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[19] += loadPerClass[j]/18.0 * mt.rand();
                phevLoad[20] += loadPerClass[j]/18.0 * mt.rand();
            }
        }else if(phevPlacement == pruning::PP_FAIR_DISTRIBUTION){

            int TotalNumbOfCustomers = 1078800;
            double percentPerBus;
            for (int j = 0; j<numClasses; j++) {
                percentPerBus = 3.5 / 100.0;
                phevLoad[0] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 3.3 / 100.0;
                phevLoad[1] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 7.7 / 100.0;
                phevLoad[2] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 1.2 / 100.0;
                phevLoad[3] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 1.9 / 100.0;
                phevLoad[4] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 5.6 / 100.0;
                phevLoad[5] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 4.4 / 100.0;
                phevLoad[6] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 7.2 / 100.0;
                phevLoad[7] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 8.2 / 100.0;
                phevLoad[8] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 9.2 / 100.0;
                phevLoad[9] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 12.1 / 100.0;
                phevLoad[10] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 6.4 / 100.0;
                phevLoad[13] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 7.9 / 100.0;
                phevLoad[14] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 2.9 / 100.0;
                phevLoad[15] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 8.0 / 100.0;
                phevLoad[16] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 6.0 / 100.0;
                phevLoad[18] += (loadPerClass[j] * percentPerBus);

                percentPerBus = 3.6 / 100.0;
                phevLoad[19] += (loadPerClass[j] * percentPerBus);

            }
        }

        //Scaling for Model
        for(int i=0; i<numBuses; i++){
            phevLoad[i] /= 100;
        }

        numPHEVs.clear();
        meanK.clear(); meanBC.clear();
        sigmaK.clear(); sigmaBC.clear();
        kMin.clear(); kMax.clear();
        bcMin.clear(); bcMax.clear();
        BC.clear(); K.clear();
        Ae.clear(); Be.clear();
        vehicleProbs.clear();
        milesDriven.clear(); neededEnergy.clear();
        energyPerMile.clear(); epsilon.clear();
        departureTime.clear(); arrivalTime.clear();
        rechargeLength.clear();
        rechargeVoltage.clear(); rechargeCurrent.clear();
        rechargeTime.clear();   loadPerClass.clear();
        epsilon.clear();

        //gsl_rng_free (r);
    }


}//end UtilsPHEV

namespace UtilsLogging{

    void getTimeStamp(char* aTime){
        time_t now;
        //char* aTime = new char[20];

        time(&now);
        struct tm * timeinfo = localtime(&now);;
        strftime(aTime, 20, "%m_%d_%Y_%H_%M_%S", timeinfo);

        //return aTime;
    }

    void writeGeneratorOutages(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
            std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement, std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads) {
        std::stringstream   ss;
        std::ofstream       myFile;
        std::string         fName;

        ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
                negateFitness, penetrationLevel, phevPlacement, numThreads);
        ss <<  "_" << aTime << "_GeneratorOutageCounts.csv";
        ss >> fName;
        myFile.open(fName.c_str(),std::ios::trunc);
        for(unsigned int i=0; i<gens.size(); i++){
            myFile << i << " ";
        }
        myFile << std::endl;
        for(unsigned int i=0; i<genOutageCounts.size(); i++){
            for(unsigned int j=0; j< genOutageCounts[i].size(); j++){
                myFile << genOutageCounts[i][j] << " ";
            }
            myFile << std::endl;
        }
        myFile.close();
    }

    std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
            std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement, int numThreads){

        std::stringstream ss(std::stringstream::in | std::stringstream::out);
        std::string fName;

        ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
                              negateFitness, penetrationLevel, phevPlacement, numThreads);
        ss << "_" << aTime << ".csv";

        ss >> fName;
        return fName;
    }
    std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod, 
            std::string pruningObj, bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement, int numThreads){

        std::stringstream   ss;
        std::string fName;

        ss << root << "/" << curSystem << "/";
        ss << samplingMethod << "_"
           << pruningMethod  << "_" 
           << curSystem      << "_"
           << pruningObj;
        
        if(useLines) ss << "_LineFailures";
        else         ss << "_NoLineFailures";

        if(multiObj) ss << "_MultiObj";
        else         ss << "_SingleObj";

        ss << "_" << classificationMethod;

        if(useLocalSearch) ss << "_LocalSearch";

        if(negateFitness) ss << "_NegObj";

        if(usePHEVs){
            ss << "_PHEVs";
            ss << "_" << UtilsGet::getPHEVPlacementString(phevPlacement);
            ss << "_" << std::fixed << std::setprecision(3) << penetrationLevel;
        }
        ss << "_OMP_" << numThreads;

        ss >> fName;
        return fName;
    }

    void writeLineOutages(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
            std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs,bool negateFitness, double penetrationLevel,
            pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > lineOutages, std::vector<Line> lines, int numThreads){


        std::stringstream   ss;
        std::ofstream       myFile;
        std::string         fName;

        ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
                negateFitness, penetrationLevel, phevPlacement, numThreads);
        

        ss <<  "_" << aTime << "_LineOutageCounts.csv";
        ss >> fName;
        
        myFile.open(fName.c_str(), std::ios::trunc);
        for(unsigned int i=0; i<lines.size(); i++){
            myFile << i << " ";
        }
        myFile << std::endl;
        for(unsigned int i=0; i<lineOutages.size(); i++){
            for(unsigned int j=0; j< lineOutages[i].size(); j++){
                myFile << lineOutages[i][j] << " ";
            }
            myFile << std::endl;
        }
        myFile.close();
    }

}//end UtilsLogging

namespace UtilsSystem{
    void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem,
                            std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& buses){
        //Clear Vectors
        gens.clear(); lines.clear(); buses.clear();

        std::ifstream myFile;
        int     count = 0,
                numGens = 0;
        myFile.open(("../Data/" + curSystem).c_str());

        if (myFile.is_open()) {
            myFile >> numGens;
            myFile >> pLoad;
            myFile >> qLoad;
            myFile >> nb;
            myFile >> nt;

            std::vector<std::string> tokens;
            std::string s;

            // Generators
            for(int i = 0; i<numGens; i++){
                myFile >> s;
                UtilsString::tokenizeString(s,tokens,",");

                gens.push_back(Generator(
                        atof(tokens[3].c_str()),
                        atof(tokens[4].c_str()),
                        atof(tokens[5].c_str()),
                        atof(tokens[6].c_str()),
                        atof(tokens[7].c_str()),
                        atoi(tokens[0].c_str()))
                    );

                gens[i].setIndex(count);
                count++;
                tokens.clear();
            }

            // Transmission Lines
            for(int i = 0; i<nt; i++){
                myFile >> s;
                UtilsString::tokenizeString(s,tokens,",");

                lines.push_back(Line(
                    atoi(tokens[0].c_str()),
                    atoi(tokens[1].c_str()),
                    atoi(tokens[2].c_str()),
                    atof(tokens[3].c_str()),
                    atof(tokens[4].c_str()),
                    atof(tokens[5].c_str()),
                    atof(tokens[6].c_str()),
                    atof(tokens[7].c_str()),
                    atof(tokens[8].c_str()),
                    atof(tokens[9].c_str()),
                    atof(tokens[10].c_str()),
                    atof(tokens[11].c_str()),
                    atof(tokens[12].c_str()),
                    atof(tokens[13].c_str()))
                );
                tokens.clear();
            }

            for(int i=0; i<nb; i++){
                myFile >> s;
                UtilsString::tokenizeString(s,tokens,",");
                //id, type, area, zone, Pd, Qd, Gs, Bs, Vm, Va, vMin, vMax, baseKva

                buses.push_back(Bus(
                    atoi(tokens[0].c_str()),
                    atoi(tokens[1].c_str()),
                    atoi(tokens[6].c_str()),
                    atoi(tokens[10].c_str()),
                    atof(tokens[2].c_str()),
                    atof(tokens[3].c_str()),
                    atof(tokens[4].c_str()),
                    atof(tokens[5].c_str()),
                    atof(tokens[6].c_str()),
                    atof(tokens[7].c_str()),
                    atof(tokens[12].c_str()),
                    atof(tokens[11].c_str()),
                    atof(tokens[9].c_str()))
                );
                tokens.clear();
            }

            myFile.close();
        }
    }


}//end UtilsSystem

namespace UtilsMath{

    std::vector<double> decToBin(double n, int numDigits) {
        std::vector<double> retVal;


        if(n <= 1) {
            retVal.push_back(n);
        }else{
            while(n > 0){
                retVal.push_back(floor(fmod(n, 2)));
                //retVal.push_back((int)n % 2);
                n = floor(n/2);  
            }
        }

        while((int)retVal.size() < numDigits){
            retVal.push_back(0);
        }
        reverse(retVal.begin(), retVal.end());
        return retVal;
    }

    //piNumber in Namespace UtilsSampling
    std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B){
        std::vector< std::vector<int> > C(A.size(), std::vector<int>(B.size(), 0));

        int sum = 0;

        for(int i=0; i<(int)A.size(); i++){
            for(int j=0; j<(int)B[0].size(); j++){
                sum = 0;       
                for(int k=0; k<(int)A[0].size(); k++){
                    sum = sum + A[i][k]*B[k][j];
                }
                C[i][j] = sum;
            }
        }
        return C;
    }

    double unitStep(double X){
        double retValue;
    
        if (X < 0.0){ retValue = 0.0;}
        else        { retValue = 1.0;}
    
        return retValue;
    }

    void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A){

        std::vector < std::vector < double > > L(A.size(),std::vector < double > (A.size(),0));
        L[0][0] = sqrt(A[0][0]);
        L[0][1] = 0;
        L[1][0] = A[0][1]/L[0][0];
        L[1][1] = sqrt(A[1][1]-(L[1][0]*L[1][0]));
        A = L;
    }

    double sigMoid(double v){
        return 1/(1+exp(-v));
    }





    int factorial(int n){
        int result=1;

        for (int i=1; i<=n; ++i){
            result=result*i;
        }

        return result;
    }

    // n Choose r
    int combination(int n, int r){
        return UtilsMath::factorial(n)/(UtilsMath::factorial(r) * UtilsMath::factorial(n-r));
    }
}//end UtilsMath

namespace UtilsString{

    void tokenizeString(std::string str,std::vector<std::string>& tokens, const std::string& delimiter = " "){

        int pos = 0;
        std::string token;

        for(;;){

            pos = str.find(delimiter);

            if(pos == (int)std::string::npos){
                tokens.push_back(str);
                break;
            }

            token = str.substr(0, pos);
            tokens.push_back(token);
            str = str.substr(pos+1);
        }
    }
    std::string vectorToString(std::vector<double> v){
        std::string result;
        char buf[1];

        result = "";
        for(unsigned int x=0; x< v.size(); x++){
            sprintf(buf, "%i", (int)v[x]);
            result += buf[0];
        }
        return result;
    }
    std::string vectorToString(std::vector<int> v){
        std::string result;

        result = "";
        for(unsigned int x=0; x< v.size(); x++){
            if(v[x] == 1){ result += '1';}
            else         { result += '0';}
        }
        return result;
    }

    std::string arrayToString(int* v, int length){
        std::string result;
        char buf[1];

        result = "";
        for(int x=0; x< length; x++){
            sprintf(buf, "%i", v[x]);
            result += buf[0];
        }
        return result;
    }
}

namespace UtilsVector {
    void printVector(std::vector < std::vector < double > >& v, std::string title, int precision){
        std::cout << title << std::endl;
        for(int i=0; i<(int)v.size(); i++){
            for(int j=0; j<(int)v[i].size(); j++){
                std::cout << setprecision(precision) << v[i][j]<< " ";
            }
            std::cout << std::endl;
        }
    }
    void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision){
        std::cout << title << std::endl;
        for(int i=0; i<(int)v.size(); i++){
            for(int j=0; j<(int)v[i].size(); j++){
                for(int k=0; k<(int)v[i][j].size(); k++){
                    std::cout << setprecision(precision) << v[i][j][k] << " ";
                }
            }
            std::cout << std::endl;
        }
    }
}

namespace UtilsSampling {

    double corputBase(double base, double number){
        double h, ib;
        double i, n0, n1;

        n0  = number; 
        h   = 0;
        ib  = 1.0/base;

        while(n0 > 0){
            n1  = (int)(n0/base);
            i   = n0 - n1 * base;
            h   = h + ib * i;
            ib  = ib / base;
            n0  = n1;
        }
        return h;
    }

    std::vector < double > haltonSample(int Nd, int Ns){
        std::vector < double > haltonSample(Nd, 0.0);
        Primes p;
        double curPrime;

        for(int j=0; j<Nd; j++){
            curPrime = p.nextPrime();
            haltonSample[j] = UtilsSampling::corputBase(curPrime, Ns+1);
        }
        return haltonSample;
    }

    // Nd - Number of dimensions,
    // Ns - Sample, Zero-based
    std::vector < double > hammersleySample(int Nd, int Ns, int totalSamples){
        std::vector < double > hSample(Nd, 0.0);
        Primes p;       
        double curPrime;

        hSample[0] = Ns/(double)totalSamples;

        for(int i=1; i<Nd; i++){ // Column
            curPrime = p.nextPrime();
            hSample[i] = UtilsSampling::corputBase(curPrime, Ns+1);         
        }
        
        return hSample;
    }
    std::string toLower(std::string str) {
        for (unsigned int i=0;i<str.length();i++){
            str[i] = tolower(str[i]);
        }
        return str;
    }
    std::string toUpper(std::string str) {
        for (unsigned int i=0;i<str.length();i++){
            str[i] = toupper(str[i]);
        }
        return str;
    }
    std::vector<std::string> permuteCharacters(std::string topermute){

        std::vector<char> iV;
        std::vector<std::string> results;
        std::string s;

        for(unsigned int x=0; x< topermute.length(); x++){
            iV.push_back(topermute[x]);
        }

        std::sort(iV.begin(), iV.end());
        s.assign(iV.begin(), iV.end());
        results.push_back(s);

        while (std::next_permutation(iV.begin(), iV.end())){
            s.assign(iV.begin(), iV.end());
            results.push_back(s);
        }
        return results;
    }
        std::vector<double> piNumbers;
    int timesPiNumberCalled = 0;
    double piNumber(int Ns){

        // Empty on First Call
        if(piNumbers.empty()){
            std::ifstream piFin;
            // double pid;
            string piVal;

            if(!piFin.is_open()){
                try{
                    piFin.open("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//piNumbers.txt");
                }catch(std::ifstream::failure e){
                    std::cerr << "Open piNumbers.txt Failed\n";
                }
            }

            if(piFin.is_open()){
                while(!piFin.eof()){
                    std::getline(piFin, piVal);
                    piNumbers.push_back(std::atof(piVal.c_str()));
                }
            }
        }

        if(timesPiNumberCalled >= (int)piNumbers.size()){
            timesPiNumberCalled = 0;
        }
        
        return piNumbers[timesPiNumberCalled++];        
    }
        // Van der Corput   - Halton in 1 dimension
    // Halton           - Use a consecutvie prime bases for each column
    // Sobol    - Faure, but uses only based 2. Each column is re-ordered based on direction numbers
    std::vector < std::vector < double > > haltonSampling(int Nd, int Ns){
        std::vector < std::vector < double > > haltonMatrix(Ns, vector<double>(Nd, 0));
        Primes p;
        double curPrime;

        for(int j=0; j<Nd; j++){
            curPrime = p.nextPrime();
            for(int i=0; i<Ns; i++){
                haltonMatrix[i][j] = UtilsSampling::corputBase(curPrime, i+1);
            } 
        }
        return haltonMatrix;
    }
        std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns){
        std::vector < std::vector < double > > hMatrix(Ns, vector<double>(Nd, 0.0));
        Primes p;
        double curPrime;

        for(int j=1; j<Nd; j++){ // Column
            curPrime = p.nextPrime();
            for(int i=1; i<Ns; i++){ // Row
                hMatrix[i][j] = UtilsSampling::corputBase(curPrime, i-1);
            }
        }
        for(int i=0; i<Ns; i++){ // Row
            hMatrix[i][0] = i/(double)Ns;
        }
        return hMatrix;
    }
        std::vector < std::vector < double > > descriptiveSampling_Random(int Nv, int Ns, MTRand& mt){
        
        std::vector < std::vector < double > > dsMatrix (Ns, vector<double>(Nv, 0));
        // double segmentSize = 1.0/(float)Ns;
        // double curSegment = 0.0;
        int a;
        double c;

        // Create Initial Matrix
        for(int i=0; i<Ns; i++){
            for(int j=0;j<Nv; j++){
                dsMatrix[i][j] = (i+1-0.5)/Ns;
            }
        }

        // Randomly match
        for(int i=0;i<Nv; i++){
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = dsMatrix[j][i]; 
                dsMatrix[j][i] = dsMatrix[a][i]; 
                dsMatrix[a][i] = c;
            }
        }

        return dsMatrix;
    }
        std::vector < std::vector < double > > latinHyperCube_Random(int Nv, int Ns, MTRand& mt){
        
        std::vector < std::vector < double > > lhsMatrix (Ns, vector<double>(Nv, 0));
        double segmentSize = 1.0/(float)Ns;
        double curSegment = 0.0;
        int a;
        double c;

        // Create Initial Matrix
        for(int i=0; i<Ns; i++){
            for(int j=0;j<Nv; j++){
                lhsMatrix[i][j] = ((i+1)-1+mt.rand())/Ns;
            }
            curSegment += segmentSize;
        }

        // Randomly match
        for(int i=0;i<Nv; i++){
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = lhsMatrix[j][i]; 
                lhsMatrix[j][i] = lhsMatrix[a][i]; 
                lhsMatrix[a][i] = c;
            }
        }

        return lhsMatrix;
        /*
        std::vector < std::vector < double > > lhsMatrix (Ns, vector<double>(Nv, 0));
        std::vector < int > idx(Ns, 0);
        int a, b, c;

        std::srand(static_cast<unsigned>(std::time(NULL)));
        for(int i=0; i<Nv; i++){
            for(int j=0; j<Ns; j++){ idx[j] = j+1;}
            
            for(int j=0; j<Ns; j++){
                a = mt.randInt(Ns-1); 
                c = idx[i]; idx[i] = idx[a]; idx[a] = c;
            }

            for(int j=0; j<Ns; j++){ lhsMatrix[j][i] = (idx[j]-1+mt.rand())/((float)Ns);}
            for(int j=0; j<Ns; j++){ lhsMatrix[j][i] = (idx[j]-0.5)/((float)Ns);}
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nv; j++){
                cout << lhsMatrix[i][j] << " ";
            }
            cout << endl;
        }

        return lhsMatrix;
        */
    }

    std::vector<int> changeBase(double num, double base, int numDigits){
        std::vector<int> retValue;
    
        do{
            retValue.push_back((int)num % (int)base);
            num = (int)(num/base); 
        }while(num != 0); 
 
        while((int)retValue.size() < numDigits){
            retValue.push_back(0);
        }

        return retValue;
    }

    std::string changeBase(std::string Base, int number){
        std::stringstream ss;

        ss << Base << number;
        return ss.str();
    }
        std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D){
        std::vector < std::vector < double > > POINTS(N, vector<double>(D, 0.0));

        //ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old.1111", ios::in);
        //ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-0.7600", ios::in);
        //ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-2.3900", ios::in);
        //ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-3.7300", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-4.5600", ios::in);
        if(!infile){
            cout << "Input file containing direction numbers cannot be found!\n";
            exit(1);
        }
        char buffer[1000];
        infile.getline(buffer, 1000,'\n');
  
        // L = max number of bits needed 
        unsigned L = (unsigned)ceil(log((double)N)/log(2.0)); 

        // C[i] = index from the right of the first zero bit of i
        unsigned *C = new unsigned [N];
        C[0] = 1;
        for (unsigned i=1;i<=N-1;i++) {
            C[i] = 1;
            unsigned value = i;
            while (value & 1) {
                value >>= 1;
                C[i]++;
            }
        } 

        // POINTS[i][j] = the jth component of the ith point
        //                with i indexed from 0 to N-1 and j indexed from 0 to D-1
        /*double **POINTS = new double * [N];
        for(unsigned i=0;i<N;i++){ POINTS[i] = new double [D];}
        for(unsigned j=0;j<D;j++){ POINTS[0][j] = 0; }*/

        // ----- Compute the first dimension -----
  
        // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
        unsigned *V = new unsigned [L+1]; 
        for (unsigned i=1;i<=L;i++){ 
            V[i] = 1 << (32-i);
            // cout << L << " " << N << " " << i << ": " << V[i] << "\n";
        } // all m's = 1

        // Evalulate X[0] to X[N-1], scaled by pow(2,32)
        unsigned *X = new unsigned [N];
        X[0] = 0;
        for (unsigned i=1;i<=N-1;i++) {
        
            X[i] = X[i-1] ^ V[C[i-1]];
            // cout << C[i-1] << " " << V[C[i-1]] << " " << X[i] << endl;
            POINTS[i][0] = (double)X[i]/pow(2.0,32); // *** the actual points
            //        ^ 0 for first dimension
        }
  
        // Clean up
        delete [] V;
        delete [] X;

        // ----- Compute the remaining dimensions -----
        for(unsigned j=1;j<=D-1;j++) {
    
            // Read in parameters from file 
            unsigned d, s;
            unsigned a;
            infile >> d >> s >> a;
            unsigned *m = new unsigned [s+1];
            for (unsigned i=1;i<=s;i++) infile >> m[i];
    
            // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
            unsigned *V = new unsigned [L+1];
            if (L <= s) {
                for (unsigned i=1;i<=L;i++) V[i] = m[i] << (32-i); 
            }
            else {
                for (unsigned i=1;i<=s;i++) V[i] = m[i] << (32-i); 
                for (unsigned i=s+1;i<=L;i++) {
                    V[i] = V[i-s] ^ (V[i-s] >> s); 
                    for (unsigned k=1;k<=s-1;k++){
                        V[i] ^= (((a >> (s-1-k)) & 1) * V[i-k]); 
                    }
                } 
            }
    
            // Evalulate X[0] to X[N-1], scaled by pow(2,32)
            unsigned *X = new unsigned [N];
            X[0] = 0;
            for (unsigned i=1;i<=N-1;i++){
                X[i] = X[i-1] ^ V[C[i-1]];
                POINTS[i][j] = (double)X[i]/pow(2.0,32); // *** the actual points
                //        ^ j for dimension (j+1)
            }
    
            // Clean up
            delete [] m;
            delete [] V;
            delete [] X;
        }
        delete [] C;
  
        return POINTS;
    }
    double *single_sobol_point(unsigned N, unsigned D){

        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old.111", ios::in);
        /*ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-0.7600", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-2.3900", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-3.7300", ios::in);
        ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-4.5600", ios::in);*/
        if(!infile){
            cout << "Input file containing direction numbers cannot be found!\n";
            exit(1);
        }
        // Gets rid of first line only
        char buffer[1000];
        infile.getline(buffer, 1000,'\n');

        // L = max number of bits needed 
        unsigned L = (unsigned)ceil(log((double)N)/log(2.0)); 
    
        // C[i] = index from the right of the first zero bit of i
        // for example 3 in binary 0011, so c[3]=3, which is the index of first zero from right
        // result of calculation is stored in an [N] array  
        unsigned *C = new unsigned [N];
        C[0] = 1;
        for (unsigned i=1;i<=N-1;i++) {
            C[i] = 1;
            unsigned value = i;
            while (value & 1) {
                value >>= 1;
                C[i]++;
            }
        }
    
        double *POINT = new double [D];
        for(unsigned j=0;j<D;j++){
            POINT[j]=0;
        }
    
        // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
        unsigned *V = new unsigned [L+1]; 
        for (unsigned i=1;i<=L;i++){ 
            V[i] = 1 << (32-i);
        } // all m's = 1
        
        // Compute first dimension of Nth row
        // Evalulate N-1th X from 0, scaled by pow(2,32), keep changing value of X
        // N must be int >=1
        unsigned X = 0; 
        for(unsigned i=1;i<=N-1;i++){   
            X = X ^ V[C[i-1]];
        }
        POINT[0] = (double)X/pow(2.0,32);
        //    ^ first dimension of Nth row 
    
        // Clean up
        delete [] V;
    
        // ----- Compute the remaining dimensions ----- j is [1, D-1]
        for(unsigned j=1;j<=D-1;j++) {
    
            // Read in parameters from file 
            unsigned d, s;
            unsigned a;
            infile >> d >> s >> a;
            unsigned *m = new unsigned [s+1];
            for (unsigned i=1;i<=s;i++) infile >> m[i];
        
            // Compute direction numbers V[1] to V[L], scaled by pow(2,32)
            unsigned *V = new unsigned [L+1];
            if (L <= s) {
                for (unsigned i=1;i<=L;i++) V[i] = m[i] << (32-i); 
            }
            else {
                for (unsigned i=1;i<=s;i++) V[i] = m[i] << (32-i); 
                for (unsigned i=s+1;i<=L;i++) {
                    V[i] = V[i-s] ^ (V[i-s] >> s); 
                    for (unsigned k=1;k<=s-1;k++){
                        V[i] ^= (((a >> (s-1-k)) & 1) * V[i-k]); 
                    }
                } 
            }
        
            // Evalulate X[0] to X[N-1], scaled by pow(2,32)    
            unsigned X =0;
            for (unsigned i=1;i<=N-1;i++){
                X = X ^ V[C[i-1]];

            }
            POINT[j] = (double)X/pow(2.0,32); // *** the actual points
                //        ^ j for dimension (j+1)
            // Clean up
            delete [] m;
            delete [] V;
        }
        delete [] C;
  
        return POINT;   
    }
        std::vector < std::vector < double > > faureSampling(int Nd, int Ns){
        std::vector < std::vector < double > >  bb(Ns, std::vector<double>(Nd ,0.0));
        std::vector < std::vector < double > >  fMatrix(Ns, std::vector<double>(Nd,0.0));
        std::vector < std::vector < int > >     a(Ns, std::vector<int>(Nd,0.0));
        std::vector < std::vector < int > > G;  
        Primes prime(Nd-1);
        double curBase;
        int r;

        curBase = prime.nextPrime();
        r       = floor(log((double)Ns)/log(curBase))+1;
        G.resize(r, std::vector<int>(r, 0.0));

        for(int j=0; j<r; j++){
            for(int i=0; i<j+1; i++){
                G[j][i] = ((int)UtilsMath::combination(j, i)) % ((int)curBase);
            }
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nd; j++){
                bb[i][j] = 1.0/pow(curBase, j+1);
            }
        }

        for(int i=0; i<Ns; i++){
            for(int j=0; j<Nd; j++){
                a[i][j] = i;
            }
        }

        for(int i=0; i<(int)a.size(); i++){
            a[i] = UtilsSampling::changeBase(i, curBase, r);
        }

        for(int i=0; i<Ns; i++){
            fMatrix[i][0] = UtilsSampling::corputBase(curBase, i);
        }

        for(int i=1; i<Nd; i++){        
            a = UtilsMath::matMult(a, G);

            for(int row=0; row<(int)a.size(); row++){
                for(int col=0; col<(int)a[row].size(); col++){
                    a[row][col] = a[row][col] % (int)curBase;
                }
            }

            for(int m=0; m<(int)fMatrix.size(); m++){
                for(int n=0; n<r; n++){
                    fMatrix[m][i] += a[m][n] * bb[m][n];
                }
            }
        }

        bb.clear(); a.clear(); G.clear();   
        return fMatrix;
    }

}//end UtilsSampling

