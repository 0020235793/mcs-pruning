/*
 * Utils.h
 *
 *  Created on: Jan 11, 2010
 *      Author: rgreen
 */

#ifndef UTILSPHEV_H_
#define UTILSPHEV_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace UtilsPHEV {
    /******************************** PHEV ********************************/
    extern void calculatePHEVLoad(
            double penetrationLevel, double rho,
            int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);

    /******************************** End PHEV ********************************/
};
#endif