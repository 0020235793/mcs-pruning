/*
 * Utils.h
 *
 *  Created on: Jan 11, 2010
 *      Author: rgreen
 */

#ifndef UTILSCOMMANDLINE_H_
#define UTILSCOMMANDLINE_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace UtilsCommandLine {

    /******************************** Command Line ********************************/
    extern void setUsage(AnyOption* opt);
    extern void setOptions(AnyOption* opt);
    /******************************** EndCommand Line ********************************/
};
#endif