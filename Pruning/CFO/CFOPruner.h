/*
 * CFOPruner.h
 *
 *  Created on: Jan 26, 2011
 *      Author: rgreen
 */

#ifndef CFOPRUNER_H_
#define CFOPRUNER_H_

#include <vector>
#include <map>

#include "Pruner.h"
#include "binaryParticle.h"
#include "UtilsSampling.h"

class CFOPruner : public Pruner {
    public:
        CFOPruner(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);

        void Init(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l,
                    double pLoad, bool ul=false);

        void Reset(int np, int nt);

        virtual ~CFOPruner();

        virtual void Prune(MTRand& mt);

    protected:

        virtual bool isConverged();
        void	calculateDiagLength();
        void 	clearVectors();
        void	clipAcceleration(int j);
        void 	evaluateFitness(int j);
        void	initA();
        void	IPD();
        void	updateAcceleration(int j);
        void	updatePositions(int j);
        void    mutate(int j);
        void    calculateDiversity(int j);
        
        bool useAccelClipping;

        int numProbesPerAxis, lastStep;
        int bestTimeStep, bestProbeNumber;
        int worstTimeStep, worstProbeNumber;
        int numEvals, numCorrections;

        double Alpha, Beta, Gamma;
        double Frep, deltaFrep;
        double bestFitness, diagLength;
        double worstFitness;
        double Denom, Numerator;
        double SumSQ, DiagLength;
        double aMax;
        double positionTime,	correctionTime, 
               fitnessTime,		accelTime, 
               shrinkTime,		convergeTime,
               totalTime, 		clippingTime;
      
        std::vector <double> xMin, xMax, diversity;
        std::vector < std::vector < std::vector < double > > > R, A;
        std::vector < std::vector < std::vector < double > > > binaryR;
        std::vector < std::vector < double > > M;
        std::vector < double >  bestFitnessArray;
        std::vector < int >		bestProbeNumberArray;

        std::string functionName;
        CStopWatch timer, timer1;
};

#endif /* PSOPRUNER_H_ */
