/*
 * MOPruner.h
 *
 *  Created on: Nov 12, 2010
 *      Author: rgreen
 */

#ifndef MOPRUNER_H_
#define MOPRUNER_H_

#include "Pruner.h"
#include "UtilsPHEV.h"
#include "UtilsMath.h"

class MOPruner : public Pruner {
	public:
		MOPruner(int np, int nt, std::vector<Generator> g, std::vector<Line> l, Classifier* opf, double pload, bool ul = false);
		std::vector<double> EvaluateSolution(std::vector<int> curSolution);
		std::vector<double> EvaluateSolution(std::string curSolution);

		virtual void Reset(int np, int nt);
		virtual void Prune(MTRand& mt);
};

#endif /* MOPRUNER_H_ */
